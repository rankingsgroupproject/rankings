from itertools import combinations
def write_to_file( name : str , data : tuple ):
    fp = open(name,"w")
    fp.write("\n".join((data)))

def parse_line( line : str) -> list:
    name , *marks = line.strip().split()
    marks = [ int(_) for _ in marks]
    return [name] + marks 

def load_data(mark_file: str) ->list:
    return [parse_line(line) for line in open(mark_file)]


class student :
    def __init__( self , data) :
        self.name , *self.marks = data
        print(self.marks)
    
    def relation( self , other) :
        if all( a > b for a ,b in zip(self.marks , other.marks )):
            return ">"
        else:
            return "#"
    
    def compare( self , other) -> str:
        return f"{self.name} {student.relation( self , other)} {other.name}"
    
    
    
    def __repr__(self):
        return f'{self.name}: {self.marks}'
    
    def __gt__(self , other) :
        return self.name if all(a > b for a, b in zip(self.marks, other.marks)) else other.name
    

class Result :
    
    def __init__(self, file):
        
        students =  load_data(file)
        
        
students = [ student(stud) for stud in load_data(r"\\wsl.localhost\Ubuntu\home\khushi\WEbootcamp\demo_rank.txt")]
print(students)
result = []

for i , j in combinations(students,2):
    result.append(student.compare(i,j))
    
print(result)




        
    

